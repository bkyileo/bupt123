__author__ = 'BK'
#encoding=utf-8
import os
'''
输入两个路径文件
输出准确率，召回率，以及F1
'''
def calAcc(infoTure,infoSubmit):
    Truth = open(infoTure)
    Submit = open(infoSubmit)

    #Truth.readline()

    labelTrue=Truth.readlines()
    labelSubmit=Submit.readlines()

    trueNum=len(labelTrue)
    submitNum=len(labelSubmit)

    rightCount=0

    labelTrueSet=set()
    for i in labelTrue:
        labelTrueSet.add(i.strip())
    for i in labelSubmit:
        if i.strip() in labelTrueSet:
            rightCount=rightCount+1

    precision=float(rightCount)/submitNum
    recall=float(rightCount)/trueNum
    F1=2*precision*recall/(precision+recall)

    print 'precision: '+str(precision)
    print 'recall: '+str(recall)
    print 'F1: '+str(F1)

path_current = os.getcwd()
path_parent = os.path.dirname(path_current)

infoTure=path_parent+"/data/sample_info.csv"
infoSubmit=path_parent+"/data/sample_info.csv"
calAcc(infoTure,infoSubmit)