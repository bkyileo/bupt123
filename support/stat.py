# -*- coding: utf-8 -*-
__author__ = 'BK'
import os
'''
1 统计类别比例
2 统计商品种类个数
3 统计商品品牌个数
4 统计四种操作的比�
'''

dataPath=os.getcwd()
parent_path = os.path.dirname(dataPath)
print parent_path
info = open(parent_path+'\data\sample_info.csv')
log = open(parent_path+'\data\sample_log.csv')

#统计不同类别(年龄段，性别)之间的个数
def labelScale():
    info.readline()
    labelDict={}
    for i in info.readlines():
        #print i
        if i.strip().split(',')[1] not in labelDict.keys():
            labelDict[i.strip().split(',')[1]]=1
        else:
            labelDict[i.strip().split(',')[1]]=labelDict[i.strip().split(',')[1]]+1
    return labelDict

#统计点击，购物车，收藏个数
def operationScale():
    log.readline()
    operationDict={}
    for i in log.readlines():
        #print i
        if i.strip().split(',')[6] not in operationDict.keys():
            operationDict[i.strip().split(',')[6]]=1
        else:
            operationDict[i.strip().split(',')[6]]=operationDict[i.strip().split(',')[6]]+1
    return operationDict

#统计商品个数，和实际交互（购物车，收藏）的个数
def catidNum():
    log.readline()
    catidset=set()
    catidDict={}
    for i in log.readlines():
        catidset.add(i.strip().split(',')[2])
        if i.strip().split(',')[6] != '0':
            if i.strip().split(',')[2] not in catidDict.keys():
                catidDict[i.strip().split(',')[2]]=1
            else:
                catidDict[i.strip().split(',')[2]]=catidDict[i.strip().split(',')[2]]+1
    return catidset,catidDict

#统计品牌个数，和实际交互（购物车，收藏）的个数
def brandidNum():
    log.readline()
    catidset=set()
    catidDict={}
    for i in log.readlines():
        catidset.add(i.strip().split(',')[4])
        if i.strip().split(',')[6] != '0':
            if i.strip().split(',')[4] not in catidDict.keys():
                catidDict[i.strip().split(',')[4]]=1
            else:
                catidDict[i.strip().split(',')[4]]=catidDict[i.strip().split(',')[4]]+1
    return catidset,catidDict

#统计用户个数，和真正交互（购物车，收藏）的个数
def userStat():
    userSetclick=set()
    userSetbuy=set()
    log.readline()
    for i in log.readlines():
        userSetclick.add(i.strip().split(',')[0])
        if i.strip().split(',')[6] != '0':
            userSetbuy.add(i.strip().split(',')[0])

    return userSetbuy,userSetclick

userSetbuy,userSetclick=userStat()
print len(userSetclick),len(userSetbuy)