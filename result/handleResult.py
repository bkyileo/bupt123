__author__ = '85359_000'
#encoding=utf-8
import os

path_current = os.getcwd()
path_parent = os.path.dirname(path_current)

#将结果写成要求的格式（userID+','+class）
def handleResult(srcPath,destPath):
    userIdList = open(path_parent+"/result/userIdList.txt")
    predict = open(path_parent+"/result/"+srcPath)
    _predict = open(path_parent+"/result/"+destPath,"w")
    alluserId = []
    for userid in userIdList:
        alluserId.append(userid.strip())
    count = 0
    for i in predict:
        _predict.write(alluserId[count]+','+i)
        count += 1
    _predict.close()
    predict.close()
    userIdList.close()

handleResult("output.txt","predict.txt")

